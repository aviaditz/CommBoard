/*
 * SDI_12.h
 *
 * Version: 0.1
 * Author:  Tom S.
 *
 * *** SDI-12 Specifications: ***
 *
 * Baud Rate:
 *   - 1200 bits per second
 *
 * Data Frame:
 *   - 10 bits per data frame
 *   - 1 start bit
 *   - 7 data bits (LSB first)
 *   - 1 even parity bit
 *   - 1 stop bit
 *
 * Data Line: SDI-12 communication uses a single bi-directional data line with
 * three-state, inverse logic.
 *
 * | LINE CONDITION | BINARY STATE | VOLTAGE RANGE     |
 * |----------------|--------------|-------------------|
 * | marking        |      1       | -0.5 to 1.0 volts |
 * | spacing        |      0       | 3.5 to 5.5 volts  |
 * | transition     |  undefined   | 1.0 to 3.5 volts  |
 *
 */ 

#ifndef SDI12_H_
#define SDI12_H_
#endif

// SDI-12 Comm. and Volt Line Ports & Pins 
#define SDI12_COM_PORT	PORTD
#define SDI12_COM_DDR	DDRD
#define SDI12_COM_PIN	PIND
#define SDI12_TX		5
#define SDI12_RX		4

#define SDI12_VOLT_PORT	PORTD
#define SDI12_VOLT_DDR	DDRD
#define SDI12_VOLT		6

// SDI-12 Settings
#define SDI12_SENSOR_TIMEOUT 20 //17 // in ms
#define SDI12_RX_ARR_SIZE    64 // in char

// PCIE2 Macros
#define PCINT2_ENABLE	PCIFR |= (1 << PCIF2); PCICR |= (1 << PCIE2)
#define PCINT2_DISABLE	PCICR &= ~(1 << PCIE2)

// Timer0 Macros
#define TIMER0_START	TCCR0B = 0x3 // CLK/64
#define TIMER0_STOP		TCCR0B = 0x0; TCNT0 = 0

// TX Line Macros
// *** NOTE *** These are all reverse logic *** NOTE ***
// Mark & space macros are used in order to avoid confusion between physical & logic layers
#define SDI12_TX_LO		SDI12_COM_PORT |= (1 << SDI12_TX)
#define SDI12_TX_HI     SDI12_COM_PORT &= ~(1 << SDI12_TX)
#define SDI12_RX_LO		SDI12_COM_PORT |= (1 << SDI12_RX)
#define SDI12_RX_HI		SDI12_COM_PORT &= ~(1 << SDI12_RX)
#define SDI12_TX_MARK   SDI12_TX_LO
#define SDI12_TX_SPACE  SDI12_TX_HI
#define SDI12_TX_IDLE   SDI12_TX_LO
#define SDI12_TX_FLOAT  SDI12_COM_DDR &= ~(1 << SDI12_TX); SDI12_COM_PORT &= ~(1 << SDI12_TX)
#define SDI12_RX_FLOAT  SDI12_COM_DDR &= ~(1 << SDI12_RX); SDI12_COM_PORT &= ~(1 << SDI12_RX)

// SDI-12 Volt Line Scripts
#define SDI12_VOLT_ON	SDI12_VOLT_PORT |= (1 << SDI12_VOLT); _delay_ms(1000)
#define SDI12_VOLT_OFF	SDI12_VOLT_PORT &= ~(1 << SDI12_VOLT)

// RX receiver status options
#define RX_TIMEOUT      0
#define RX_RECEIVING	1
#define RX_PARITY_ERR	2
#define RX_MSG_DONE		3

#ifndef F_CPU
#define F_CPU 8000000UL

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <util/parity.h>
#include <util/atomic.h>
#include <stdbool.h>
#include <string.h>

void SDI12Setup();
void SDI12WakeUpSensors(uint8_t extraDelayTime);//in ms
void SDI12SendCommand(char outCharArr[]);
void sendChar(char outChar);
uint8_t SDI12GetResponse(void); // internal
void dynamicDelay(uint8_t delayms);
uint8_t SDI12GetRXStatus(void);
char* SDI12GetSensorMsg(void); // for user

#endif /* SDI-12_H_ */