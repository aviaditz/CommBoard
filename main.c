/*
 * UART_to_I2C_Sensibus_Bridge.c
 * Rev. 3 (03/14/2021)
 *
 * Created: 11/14/2021 2:51:43 PM
 * Author : Tom S.
 * Most of the I2C + Sensibus code implemented by Haim Rozenboim. 
 * SDI-12 lib. + integration done by Tom.
 *
 * UART RX & TX buffers = 64 chars, if required change in uart.h
 */ 

#include "main.h"
#include "sensirion.h"
#include <util/delay.h>

void I2C_RW_TEST( void);
char uart_rx_buffer[BUFFER_SIZE];
uint8_t uart_rx_buffer_index = 0;
RingBuff_t uart_tx_buffer;
RingBuff_Data_t uart_tx_buffer_data[BUFFER_SIZE];
uint8_t parse_buffer_bool = FALSE;
uint8_t Work_Mode= IDLE;
uint8_t is_string_Equal( char *s,char *p);
uint8_t test;
uint8_t err=0;
uint8_t buff[BUFFER_SIZE];

#define EEPROM_ADDR_UART_BAUDRATE (0)
uint32_t baud=UART_BAUD_RATE;
uint32_t baud_delay = 64000000/UART_BAUD_RATE;
void init_avr_uart(){
	baud=eeprom_read_dword(EEPROM_ADDR_UART_BAUDRATE);	
	if((baud>115200)||(baud<4800)||(baud%4800!=0)){
		baud = UART_BAUD_RATE;
		eeprom_write_dword(EEPROM_ADDR_UART_BAUDRATE,baud);
	}
	baud_delay = 64000000/(baud);
	uart_init(UART_BAUD_SELECT(baud, F_CPU));
}
//************************************************************************
int main(void)
{
	shtInit();
	RingBuffer_InitBuffer(&uart_tx_buffer); // init uart tx buffer
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
	eeprom_busy_wait();
	init_avr_uart();
	i2c_init();
	uart_send_fw_version();
	SDI12Setup();
	Pram.SDI_wakeupstatus = 0;
	sei();
		
    while(1) 
    {
		uart_service_routine();
		parse_buffer_handler();
		// int16_t xxx=	readTemperatureRaw();
		// TestI2CADD( );
	
		switch (Work_Mode)
		{
		case I2C_W:
			SET_I2C;
			err = I2c_Write_Data();
			send_i2C_staus_to_UART();
			Work_Mode = IDLE;
			break; 
	
		case I2C_R:
			SET_I2C;
			I2c_Read_Data();
			send_i2C_staus_to_UART();
			
			if (Pram.ERROR == 0) 
			{ 
				send_i2C_read_to_UART();
			}
			
			Work_Mode = IDLE;
			break;
			 
		case I2C_C:
			Work_Mode = IDLE;
			break;
				 
		case Sensibus_R:
			SET_I2C_to_GPIO;
			Sensibus_read_Data();
			send_Sensibus_staus_to_UART();
							
//			if(SENbus.ACK_Status == 0)
//			{
//				send_i2C_read_to_UART();
//			}
			
			Work_Mode = IDLE; 
			break; 
     
		case Sensibus_W : 
			SET_I2C_to_GPIO;
			Sensibus_write_Data();
			send_Sensibus_staus_to_UART();
			Work_Mode = IDLE; 
			break; 

		case Board_Query:
			uart_send_fw_version();
			Work_Mode = IDLE;  
			break;
			 
		case IDLE:  
			break;
			
		case RxERROR: 
			send_i2C_staus_to_UART(); 
			break;
			
		case SDI12P: // SDI-12 12V power
			if(Pram.SDI12_12Vstate == 0)
			{
				SDI12_VOLT_OFF;
				ring_buffer_insert_string(&uart_tx_buffer, "12V OFF\0");
			}
			else if (Pram.SDI12_12Vstate == 1)
			{
				//SDI12_VOLT_ON;
				SDI12_VOLT_PORT |= (1 << SDI12_VOLT); 
				_delay_ms(1000);
				ring_buffer_insert_string(&uart_tx_buffer, "12V ON\0");
				Pram.SDI_wakeupstatus=0;
			}
			
			Work_Mode = IDLE;
			break;
			
		case SDI12W: // SDI-12 write & read			
			if(Pram.SDI_wakeupstatus == 0)
			{
				Pram.SDI_wakeupstatus = 1;
				SDI12WakeUpSensors(0);
			}else{
				Pram.SDI_wakeupstatus *= 10;
			}
			
			SDI12SendCommand(Pram.W_Data);
			Pram.sdi_status = SDI12GetResponse();
			
			switch(Pram.sdi_status)
			{
				case RX_TIMEOUT:
					ring_buffer_insert_string(&uart_tx_buffer, "Sensor timed out\0");	
					break;
			
				case RX_PARITY_ERR:
					ring_buffer_insert_string(&uart_tx_buffer, "Parity error\0");
					break;
			
				case RX_MSG_DONE:
					ring_buffer_insert_string(&uart_tx_buffer, SDI12GetSensorMsg());
					break;
			}
			
			Pram.SDI_wakeupstatus = 0;
			Work_Mode = IDLE;
			break;
				
		} // switch
	} // while
} // main

//************************************************************************
void parse_buffer_handler(void)
{
	if(parse_buffer_bool != FALSE) 
	{	
		//ATOMIC_BLOCK(ATOMIC_RESTORESTATE){
		parse_buffer();
		parse_buffer_bool = FALSE;
		
	}
	
	while(!RingBuffer_IsEmpty(&uart_tx_buffer))
	{
		uart_putc(RingBuffer_Remove(&uart_tx_buffer));
	}
}

//************************************************************************
void uart_service_routine()
{
	int size=0;
	uint16_t Rx_data = uart_getc();
    
	if(Rx_data & UART_NO_DATA)
		return;

		uart_rx_buffer[uart_rx_buffer_index++] = (unsigned char)Rx_data;
		//ring_buffer_insert_string(&uart_tx_buffer, "N\0");
		if ((unsigned char)Rx_data == CR) // string termination -> parse cmd
		{
			parse_buffer_bool = TRUE;			
		}
}

//*************************************************************************************
void inline uart_send_fw_version()
{
	ring_buffer_insert_string(&uart_tx_buffer, "VER");
	ring_buffer_insert_string(&uart_tx_buffer, CURRENT_FW);
	RingBuffer_Insert(&uart_tx_buffer, '\0');	
	while(!RingBuffer_IsEmpty(&uart_tx_buffer))
	{
		uart_putc(RingBuffer_Remove(&uart_tx_buffer));
	}
}

//*************************************************************************************
void send_i2C_read_to_UART(void)
{
	uint8_t size=32;
	uint8_t i;
	int cx;
	int buff_offset=0;

	// buff_offset=snprintf(buff,size,"i2c:");
	for ( i = 0; i < Pram.byte_count; i++) 
	{
		cx=	snprintf(buff+buff_offset, size, "%02x", Pram.R_Data[Pram.byte_count-1-i]);
		buff_offset+=cx;
	}
	
	cx=	snprintf(buff+buff_offset, size,0);
	buff[buff_offset]=0;
	buff_offset+=cx-1;
	ring_buffer_insert_string_VAR(&uart_tx_buffer,buff,buff_offset);
	uart_putc(0);
}

//*************************************************************************************
void send_i2C_staus_to_UART(void)
{
	if(Pram.ERROR == 0)	{ring_buffer_insert_string(&uart_tx_buffer, "OK \0");}
	else				{ring_buffer_insert_string(&uart_tx_buffer, "ER \0");}

}

//*************************************************************************************
void send_Sensibus_staus_to_UART(void)
{
//	if (SENbus.ACK_Status==0)
//		{ring_buffer_insert_string(&uart_tx_buffer, "OK \0");}
//	else      
//	  {ring_buffer_insert_string(&uart_tx_buffer, "ER \0");}
	if((Pram.R_Data[0]==0xFF) &&(Pram.R_Data[1]==0xFF)){
		ring_buffer_insert_string(&uart_tx_buffer, "ER00\0");		
	}else if (SENbus.ACK_Status==0){
		send_i2C_read_to_UART();
	}else{
		char buffer [6];
		snprintf(buffer, 100,"ER%2d",SENbus.ACK_Status);	
		ring_buffer_insert_string(&uart_tx_buffer, buffer);
	}
	parse_buffer_handler();
}

//*************************************************************************************
void ring_buffer_insert_string(RingBuff_t* const Buffer, char *s)
{
	for (uint8_t i = 0; i <= strlen(s); i++)
	{
		RingBuffer_Insert(Buffer, s[i]);
	}
}

//*************************************************************************************
void ring_buffer_insert_string_VAR(RingBuff_t* const Buffer, char *s,int size)
{
	for (uint8_t i = 0; i < size; i++)
	{
		RingBuffer_Insert(Buffer, s[i]);
	}
}

//*************************************************************************************
void parse_buffer()
{
	char local_buffer[BUFFER_SIZE];
	char str[4];
	uint16_t temp;
	strcpy(local_buffer, uart_rx_buffer); // copy from global to local buffer
	memset(uart_rx_buffer, 0, sizeof(uart_rx_buffer)); // empty global buffer
	uart_rx_buffer_index = 0;                          // reset index
	
		 if			(is_string_Equal(local_buffer,"iS"))  {Work_Mode=I2C_W;          }
	else if			(is_string_Equal(local_buffer,"iR"))  {Work_Mode=I2C_R;          }
	else if			(is_string_Equal(local_buffer,"bS"))  {Work_Mode=Sensibus_W;     }
	else if			(is_string_Equal(local_buffer,"bR"))  {Work_Mode=Sensibus_R;     }
	else if			(is_string_Equal(local_buffer,"Ver")) {Work_Mode=Board_Query;    }
	else if			(is_string_Equal(local_buffer,"iC"))  {Work_Mode=I2C_speed_grade;}
	else if         (is_string_Equal(local_buffer,"Sp"))  {Work_Mode=SDI12P;         }
	else if         (is_string_Equal(local_buffer,"Sw"))  {
		Work_Mode=SDI12W;         }
		
	temp=hex2char (local_buffer[2])<<4;
	temp+=hex2char (local_buffer[3]);
	Pram.ID=temp;
	
	temp=hex2char (local_buffer[4])<<4;
	temp+=hex2char (local_buffer[5]);
	Pram.MSB_address=temp;
	
	if(Work_Mode == SDI12P)
	{
		Pram.SDI12_12Vstate = local_buffer[2] - '0';
	}
	
	temp=hex2char (local_buffer[6])<<4;
	temp+=hex2char (local_buffer[7]);
	Pram.LSB_address=temp;
	
	temp=hex2char (local_buffer[8])<<4;
	temp+=hex2char (local_buffer[9]);
	Pram.byte_count=temp;
	
	if((Work_Mode == SDI12W) || (Work_Mode == SDI12P))
	{
		uint8_t i = 0;
		
		while(local_buffer[2 + i] != '\0')
		{
			Pram.W_Data[i] = local_buffer[2 + i];
			i++;
		}
		
		Pram.W_Data[i] = local_buffer[2 + i];
	}
	else
	{
		for(int i=0;i<Pram.byte_count;i++)
		{
			temp=hex2char (local_buffer[i*2+10])<<4;
			temp+=hex2char (local_buffer[i*2+11]);
			Pram.W_Data[i]=temp;
		}
	}
}

//*************************************************************************************
uint16_t hex2char (char Data)
{
	uint16_t RETVAL;
		switch ( Data)
			{
				case '0':RETVAL=0;break;
				case '1':RETVAL=1;break;
				case '2':RETVAL=2;break;
				case '3':RETVAL=3;break;
				case '4':RETVAL=4;break;
				case '5':RETVAL=5;break;
				case '6':RETVAL=6;break;
				case '7':RETVAL=7;break;
				case '8':RETVAL=8;break;
				case '9':RETVAL=9;break;
				case 'a':RETVAL=10;break;
				case 'b':RETVAL=11;break;
				case 'c':RETVAL=12;break;
				case 'd':RETVAL=13;break;
				case 'e':RETVAL=14;break;
				case 'f':RETVAL=15;break;
				case 'A':RETVAL=10;break;
				case 'B':RETVAL=11;break;
				case 'C':RETVAL=12;break;
				case 'D':RETVAL=13;break;
				case 'E':RETVAL=14;break;
				case 'F':RETVAL=15;break;
				default:RETVAL=0Xff;break;		
			}
			
	return RETVAL;
}
void Sensibus_write_Data (void ) 
{
	write_register_Raw(Pram.LSB_address,Pram.W_Data[0] );
}

void Sensibus_read_Data (void )
{
	uint16_t Data;
	SENbus.ACK_Status = 0;
	Pram.R_Data[0]=0xff;
	Pram.R_Data[1]=0x07;
	Data=	 read_register_Raw(Pram.LSB_address );
	Pram.byte_count = 2;
	Pram.R_Data[0]=	 (uint8_t) Data;
	Pram.R_Data[1]=	 (uint8_t) (Data>>8);
;
}

void I2c_Read_Data (void )
{
	i2c_start_wait(Pram.ID );
	if (Pram.ERROR==1){return;}
	if (Pram.MSB_address==0)
	{
		i2c_write(Pram.LSB_address);
	}
	else
	{
		i2c_write(Pram.MSB_address);
		i2c_write(Pram.LSB_address);
	}
	for(int i=0;i<Pram.byte_count;i++)
	{
		i2c_rep_start(Pram.ID + 1);
		Pram.R_Data[i]=	i2c_readNak();
	}
	i2c_stop();
}

unsigned char I2c_Write_Data (void )
{
	unsigned char REVAL=0;

	REVAL= i2c_start(Pram.ID & 0xFE);
		if (Pram.ERROR==1){return;}
	if (Pram.MSB_address==0)
	{
	i2c_write(Pram.LSB_address);	
	}
	else
	{
		i2c_write(Pram.MSB_address);
		i2c_write(Pram.LSB_address);
	}
	
	for(int i=0;i<Pram.byte_count;i++)
	{
		i2c_write(Pram.W_Data[0]);
	}
	
	i2c_stop();
	return REVAL;
}

//*******************************************************
uint8_t I2C_ACK_status[127] ;
void   TestI2CADD( void)
//*******************************************************
{
	uint8_t ADDW;
	uint8_t ADDR;
	uint8_t JJ;

	Pram.MSB_address=0;
	for( JJ=0; JJ<128;JJ++)
	{
		Pram.W_Data[0]=0xaa;
		Pram.LSB_address=0;
		ADDW=JJ*2;
		Pram.ID=ADDW;
		I2c_Write_Data ( );
		I2C_ACK_status[ADDW/2] =Pram.ERROR;
		Pram.ID=ADDW;
		//I2C_status[ADDW] =I2c_Read_Data ( );
		
	}
}

//*******************************************************
void I2C_RW_TEST(void)
{
	uint8_t ADDW;
	uint8_t ADDR;
	uint8_t JJ;
	uint8_t Write_data[3];
	
	Pram.MSB_address=0;
	Pram.ID=48;
	Pram.W_Data[0]=0xaa;Pram.W_Data[1]=0x55;Pram.W_Data[2]=0x21;
	Pram.LSB_address=0x20;
	Pram.byte_count=0x05;
	ADDW =I2c_Write_Data();
	I2c_Read_Data();
}

uint8_t is_string_Equal( char *s,char *p)
{
	uint8_t REVAL=0;
	if ((*s == *p)&& (*(s+1) == *(p+1))){REVAL=1;}
	return REVAL;	
}

void print_float(float data)
{	
	int buff_offset;
	char buffer [100];
	buff_offset=	snprintf(buffer, 100,"%f",data);	
	ring_buffer_insert_string_VAR(&uart_tx_buffer,buffer,buff_offset);
}

void print_VAR(int data)
{
	int buff_offset;
	char buffer [100];
	buff_offset=	snprintf(buffer, 100,"%d",data);	
	ring_buffer_insert_string_VAR(&uart_tx_buffer,buffer,buff_offset);
}
