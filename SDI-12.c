/*
 * SDI_12.c
 *
 * Version: 0.1
 * Author: Tom S.
 */ 

// Data frame (RX bits)
#define START_BIT	-1
#define BIT0		0
#define BIT1		1
#define BIT2		2
#define BIT3		3
#define BIT4		4
#define BIT5		5
#define BIT6		6
#define PARITY_BIT	7
#define STOP_BIT	8

#include "SDI-12.h"
#include <util/delay.h>

uint8_t RXReceiverStatus;
static char RXArray[SDI12_RX_ARR_SIZE];
uint8_t RXArrayIndex;
int8_t currentBit;
char tempChar;
volatile uint8_t timer0ISR = 0;
volatile uint8_t PCINTFlag;

void SDI12Setup()
{
	// ports & pins setup
	SDI12_VOLT_DDR |= (1 << SDI12_VOLT);
	SDI12_VOLT_OFF;
	
	SDI12_COM_DDR |= (1 << SDI12_TX);
	SDI12_COM_DDR &= ~(1 << SDI12_RX);
	SDI12_TX_IDLE;
	SDI12_RX_FLOAT;
	
	// timer0 setup
	power_timer0_enable();
	TIMSK0 = (1 << OCIE0A); // compare match A int. enable
	TCCR0A = (1 << WGM01);  // CTC mode
	//OCR0A = 207;          // int. every bit length (for CLK = 16MHz, timer0/64)
	OCR0A = 105;            // int. every bit length (for CLK = 8MHz, timer0/64)
	
	// PCINT setup
	PCMSK2 |= (1 << PCINT20); // PCI on D4 pin
}

/*
 * Wakes up all the sensors on the SDI-12 bus
 * If more time required than the default 12ms, call with extraDelayTime > 0 [ms]
 */
void SDI12WakeUpSensors(uint8_t extraDelayTime)
{
	SDI12_TX_SPACE; // break
	//_delay_ms(12);
	_delay_ms(12.5);
	dynamicDelay(extraDelayTime);
	
	SDI12_TX_MARK;
	//_delay_ms(8.33);
	_delay_ms(8.4);
}

/*
 * Generates delay [ms]
 * Used in order to avoid compile time error for _delay_ms())
 */
inline void dynamicDelay(uint8_t delayms)
{
	for(uint8_t i = 0; i < delayms; i++)
	{
		_delay_ms(1);
	}
}

/*
 * Sends a command over SDI-12 bus (gets a String)
 * Does not wake up sensors
 */
void SDI12SendCommand(char outCharArr[])
{
	uint8_t i = 0;
	
	while(outCharArr[i] != '\0')
	{
		sendChar(outCharArr[i]);
		i++;
	}
	
	SDI12_TX_IDLE;
}
#ifdef NDEBUG
	#define SDI12_BIT_DURATION 833
#else
	#define SDI12_BIT_DURATION 761
#endif
/*
 * Sends a single character over SDI-12 bus
 * Generates start & stop (spacing & marking) bits
 */
void sendChar(char outChar)
{
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		outChar |= (parity_even_bit(outChar) << 7); // insert even parity bit
		
		SDI12_TX_SPACE; _delay_us(SDI12_BIT_DURATION); // spacing (start bit)
	
		for(uint8_t i = 0; i < 8; i++)
		{
			if((outChar >> i) & 0x1) SDI12_TX_MARK;
			else                     SDI12_TX_SPACE;
			_delay_us(SDI12_BIT_DURATION);
		}
	
		SDI12_TX_MARK; _delay_us(SDI12_BIT_DURATION); // marking (stop bit)
	}
}

/*
 * Enables timeout timer & PCINT on RX line
 * Returns RX status (see defines at the top of this file)
 */
uint8_t SDI12GetResponse(void)
{
	uint8_t SDI12TimeoutCounter = 0;
	
	tempChar = 0;
	memset((void *)RXArray, '\0', SDI12_RX_ARR_SIZE);
	RXArrayIndex = 0;
	RXReceiverStatus = RX_RECEIVING;
	currentBit = START_BIT;
	PCINTFlag = 0;
	
	PCINT2_ENABLE;
	
	// wait for timeout, break if gets data from sensor
	while(!PCINTFlag)
	{	
		if(SDI12_COM_PIN & (1 << SDI12_RX)){
//			PCINTFlag=1;
		}
		if(SDI12TimeoutCounter >= SDI12_SENSOR_TIMEOUT)
		{
			RXReceiverStatus = RX_TIMEOUT;
			break;
		}
		
		_delay_ms(1);
		SDI12TimeoutCounter++;
	}
	
	// getting msg, all word done in timer0 interrupt
	while(RXReceiverStatus == RX_RECEIVING)
	{
		if(timer0ISR == 1)
		{
			timer0ISR = 0;
			
			switch(currentBit)
			{
				case START_BIT:
				currentBit++;
				break;

				case BIT0:
				case BIT1:
				case BIT2:
				case BIT3:
				case BIT4:
				case BIT5:
				case BIT6:
					if(SDI12_COM_PIN & (1 << SDI12_RX))
					{
						tempChar |= (1 << currentBit);
					}
			
					currentBit++;
					break;
			
				case PARITY_BIT:
					if(parity_even_bit(tempChar) && ((SDI12_COM_PIN & (1 << SDI12_RX)) == 0))
					{
						RXReceiverStatus = RX_PARITY_ERR;
						TIMER0_STOP;
					}
			
					currentBit++;
					break;
			
				case STOP_BIT:
					RXArray[RXArrayIndex++] = tempChar;
			
					if(tempChar == '\n') // <LF> = termination
					{
						PCINT2_DISABLE;
						TIMER0_STOP;
				
						RXReceiverStatus = RX_MSG_DONE;
					}
			
					tempChar = 0;
					currentBit = START_BIT;
					break;
			} // switch
		} // if
	} // while
	
	return RXReceiverStatus;
}

/*
 * Returns the last RX status
 */
uint8_t SDI12GetRXStatus(void)
{
	return RXReceiverStatus;
}

char* SDI12GetSensorMsg(void)
{
	return RXArray;
}

ISR(PCINT2_vect)
{	
	// start bit detection (according to RX signal polarity)
	if((SDI12_COM_DDR & (1 << SDI12_RX)) == 0)
	{
		PCINTFlag = 1;
		TIMER0_START;
	}
	
	//TCNT0 = 103; // re-sync. (start timer0 at 1/2 bit length offset)
	TCNT0 = 51; // re-sync. (start timer0 at 1/2 bit length offset)
}

ISR(TIMER0_COMPA_vect)
{
	timer0ISR = 1;
}