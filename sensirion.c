/**
 * sensirion_h Library
 *
 * Hydrosense 2016 / hydrosense.net
 *
 * Based on previous work by:
 *    Jonathan Oxer <jon@oxer.com.au> / <www.practicalarduino.com>
 *    Maurice Ribble: <www.glacialwanderer.com/hobbyrobotics/?p=5>
 *    Wayne ?: <ragingreality.blogspot.com/2008/01/ardunio-and-sht15.html>
 *
 * Manages communication with SHT1x series (SHT10, SHT11, SHT15, SHT31)
 * temperature / humidity sensors from Sensirion (www.sensirion.com).
 */

#include "main.h"
#include "sensirion.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <util/delay.h>
#include <util/atomic.h>



#define  LOW 0
#define HIGH 1
#define  OUTPUT 1
#define  INPUT 0
#define MSBFIRST 1
#define LSBFIRST 0
#define SCK_H  PORTC |= 0x20// set bit 5 
#define SCK_L  PORTC &= 0xDF// reset bit 5
#define SCK_O  DDRC |= 0x20 // set output 
#define SCK_Z  DDRC &= 0xDF // set input 

#define SDA_H  PORTC |= 0x10
#define SDA_L  PORTC &= 0xEF
#define SDA_Z  DDRC &= 0xEF
#define SDA_O  DDRC |= 0x10
#define dataPin 0  //PORTC pin 4  UNO D19
#define clockPin 1  //PORTC pin 5  UNO D18
#define millis() 2000
#define  AUTO_PULLUP MCUCR |= PUD


void	 pinMode(uint8_t Pin, uint8_t level);
/* ================  Public methods ================ */
void shtInit(void)
{

}
/**
 * Reads the current temperature in degrees Celsius
 */

inline void shtDelay(uint16_t arg)
{
	// shtDelay in each clock state. clock period is twice this time.
	// 100 us = 5 KHz
	// 500 us = 0.5 KHz
	// the datasheet says you can go to 1 MHz, but in practice
	// a few KHz seems to be the most reliable.
	//_delay_us(1);//200/(CLOCK_FACTOR)
	_delay_loop_1(arg*96);
}

float readTemperatureC(void)
{
  uint16_t _val;                // Raw value returned from sensor
  float _temperature;      // Temperature derived from raw value

  // Conversion coefficients from SHT15 datasheet
  const float D1 = -40.0;  // for 14 Bit @ 5V
  const float D2 =   0.01; // for 14 Bit DEGC

  // Fetch raw value
  _val = readTemperatureRaw();

  // Convert raw value to degrees Celsius
  _temperature = (_val * D2) + D1;

  return (_temperature);
}

/**
 * Reads the current temperature in degrees Fahrenheit
 */
float readTemperatureF(void)
{
  uint16_t _val;                 // Raw value returned from sensor
  float _temperature;       // Temperature derived from raw value

  // Conversion coefficients from SHT15 datasheet
  const float D1 = -40.0;   // for 14 Bit @ 5V
  const float D2 =   0.018; // for 14 Bit DEGF

  // Fetch raw value
  _val = readTemperatureRaw();

  // Convert raw value to degrees Fahrenheit
  _temperature = (_val * D2) + D1;

  return (_temperature);
}



float readHumidity(void)
{
	uint16_t _val;                    // Raw humidity value returned from sensor
	float _linearHumidity;       // Humidity with linear correction applied
	float _correctedHumidity;    // Temperature-corrected humidity
	float _temperature;          // Raw temperature value

	// Conversion coefficients from SHT15 datasheet
	const float C1 = -4.0;       // for 12 Bit
	const float C2 =  0.0405;    // for 12 Bit
	const float C3 = -0.0000028; // for 12 Bit
	const float T1 =  0.01;      // for 14 Bit @ 5V
	const float T2 =  0.00008;   // for 14 Bit @ 5V

	// Command to send to the SHT1x to request humidity
	const uint8_t _gHumidCmd = 0b00000101;

	// check cache
	//if ( millis() - _lastHumMillis < SHT_CACHE_MILLIS){
//		return _lastHumVal;

	//	} else
		{
		// Fetch the value from the sensor
		if ( sendCommandSHT(_gHumidCmd ) == SHT_SUCCESS )
		{
			waitForResultSHT();
			_val = getData16SHT();
			
			uint8_t rxcrc = rev8bits( readCRC( ) );
			uint8_t mycrc = crc8add( 0, _gHumidCmd );
			mycrc = crc8add( mycrc, _val );
			//if (mycrc != rxcrc){
			//	Serial.println("SHT: crc error");
			//	Serial.print(" Got:      0x"); Serial.println(rxcrc, HEX);
			//	Serial.print(" Expected: 0x"); Serial.println(mycrc, HEX);
	//			return NAN;
		//	}

			// Apply linear conversion to raw value
			_linearHumidity = C1 + C2 * _val + C3 * _val * _val;

			// Get current temperature for humidity correction
			_temperature = readTemperatureC();

			// Correct humidity value for current temperature
			_correctedHumidity = (_temperature - 25.0 ) * (T1 + T2 * _val) + _linearHumidity;

			// cache
			_lastHumVal = _correctedHumidity;
		//	_lastHumMillis = millis();

			return (_correctedHumidity);
		}
	}
	//error
	return NAN;
}




//***************************************************************************************************
uint16_t readTemperatureRaw(void)
{
	uint16_t _val;

	// Command to send to the SHT1x to request Temperature
	const uint8_t _gTempCmd  = 0b00000011;

	if (millis() - _lastTempMillis < SHT_CACHE_MILLIS )
	{
		return _lastTempRaw;
		}
		 else
		  {
		if ( sendCommandSHT(_gTempCmd ) == SHT_SUCCESS )
		{
			waitForResultSHT();
			_val = getData16SHT();
			uint8_t rxcrc = rev8bits( readCRC( ) );
			uint8_t mycrc = crc8add( 0, _gTempCmd );
			mycrc = crc8add( mycrc, _val );
//			if (mycrc != rxcrc){
	//			Serial.println("SHT: crc error");
	//			Serial.print(" Got:      0x"); Serial.println(rxcrc, HEX);
	//			Serial.print(" Expected: 0x"); Serial.println(mycrc, HEX);
	//			return 0;
			
			// cache
			_lastTempMillis = millis();
			_lastTempRaw = _val;
			return (_val);
		}
	}
	return 0; // error
}
//**********************************************************************************
int8_t readStatus(void)
{
  uint16_t _val;

  // Command to send to the SHT1x to request Status
  const uint8_t _gStatCmd  = 0b00000111;

  sendCommandSHT(_gStatCmd);
  // result is immediate with read status.
  //waitForResultSHT(_dataPin);

  shtDelay(1);
  _val = getData16SHT();
  /*
  // Send the required ack
  digitalWrite(_dataPin, LOW);
  pinMode(_dataPin, OUTPUT);
  shtDelay(1);
  digitalWrite(_clockPin, HIGH);
  shtDelay(1);
  digitalWrite(_clockPin, LOW);
  shtDelay(1);
  pinMode(_dataPin, INPUT);
  */

  // end transmission, turn off gpio's
  pinMode(dataPin, INPUT);
  pinMode(clockPin, INPUT);
//digitalWrite(dataPin, LOW);
  uint8_t rx_val = _val >> 8;
  uint8_t rx_crc = _val & 0xff;
  uint8_t local_crc = crc8add( 0, _gStatCmd );
  local_crc = crc8add(local_crc, rx_val);

  if (rx_crc != rev8bits(local_crc)){
//	Serial.println("SHT: crc error");
//	Serial.print(" Got:      0x"); Serial.println(rx_crc, HEX);
//	Serial.print(" Expected: 0x"); Serial.println(local_crc, HEX);
  }

  return rx_val;
}

/**
*/


//**********************************************************************
void	 digitalWrite(uint8_t Pin, uint8_t level)
{
if (Pin==dataPin)
	{
	//if (level==HIGH) {SDA_H;}	else {SDA_L;}
	if (level==HIGH){ SDA_Z; SDA_L;}	
		else		{ SDA_O; SDA_L;}
	}
	else 
	{
//		if (level==HIGH) {SCK_H;}else {SCK_L;}
	if (level==HIGH){ SCK_Z; SCK_L;}
	else			{ SCK_O; SCK_L;}
	}
}
inline void	 pinMode(uint8_t Pin, uint8_t level)
{
if (Pin==dataPin)
{
	if (level==OUTPUT) {SDA_O;}else {SDA_Z;}
}
else 
{
	if (level==OUTPUT) {SCK_O;}else {SCK_Z;}
		shtDelay(20);//_delay_ms(1);//CLOCK_FACTOR
}


}
uint8_t digitalRead(uint8_t Pin) 
{
if (Pin==dataPin)	
	{
	if ((PINC & 0x10)!=0)	{return 1;}
	}
	return 0;
}

//****************************************************************************
	/*
 * if ack == SHT_ACK, the return value is 0 on success, 1 if there was no ack.
 */
//uint8_t shiftOut(uint8_t dataPin, uint8_t clockPin, uint8_t bitOrder, uint8_t val, uint8_t ack)
uint8_t shiftOut( uint8_t bitOrder, uint8_t val, uint8_t ack)

{
	uint8_t i;
	uint8_t Flag;
	uint8_t rval = SHT_SUCCESS;

	for (i = 0; i < 8; i++)
	  {
		if (bitOrder == LSBFIRST)
			digitalWrite(dataPin, !!(val & (1 << i)));
		else
		Flag=!!(val & (1 << (7 - i)));
			digitalWrite(dataPin, !!(val & (1 << (7 - i))));

		shtDelay(1);
		digitalWrite(clockPin, HIGH);
		shtDelay(1);
		if ((ack == SHT_ACK) && (i == 7))
		 { // last byte prepare for ACK
			pinMode(dataPin, INPUT);
			//digitalWrite(dataPin, LOW);
			AUTO_PULLUP;
		}
		digitalWrite(clockPin, LOW);
	}

	if (SHT_ACK == ack)
	{
	  shtDelay(1);
	  // Verify we get the correct ack

	  // sensor should be pulling data pin LOW
	  ack = 0;
	  while ((ack < 100) && (digitalRead(dataPin) != LOW)) {
	    ack++;
	    shtDelay(1);
	  }
	  if (ack>=10){
//	    Serial.println("Ack error 0");
	    rval = SHT_FAIL;
	  }
	  digitalWrite(clockPin, HIGH);
	  shtDelay (1);
	  digitalWrite(clockPin, LOW);
	}
	SENbus.ACK_Status=ack;
	return rval;
} 
uint8_t sendCommandSHT(uint8_t _command)
{
	if (sendCommandSHT_internal(_command) != 0)
	{
		// no ack, reset the interface and retry ONE time
		digitalWrite(dataPin, HIGH);
		digitalWrite(clockPin, LOW);
		pinMode(dataPin, OUTPUT);
		pinMode(clockPin, OUTPUT);
		digitalWrite(dataPin, HIGH);
		digitalWrite(clockPin, LOW);

		shtDelay(1);
		// send >9 clocks to reset interface
		shiftOut( MSBFIRST, 0xff, SHT_NOACK);
		shtDelay(1);
		shiftOut( MSBFIRST, 0xff, SHT_NOACK);
		// if we reset, do a real 1 ms delay
		shtDelay(20);//_delay_ms(1);//CLOCK_FACTOR

//		return sendCommandSHT_internal(_command, _dataPin, _clockPin);
		return sendCommandSHT_internal(_command);
	} else{
		// success
		return SHT_SUCCESS;
	}
}
/**
 */
void waitForResultSHT(void)
{
  int i;
  int ack;

  pinMode(dataPin, INPUT);
  //digitalWrite(dataPin, LOW);
  
  for(i= 0; i < (1000*CLOCK_FACTOR); ++i)
  {
    shtDelay(10);
    ack = digitalRead(dataPin);

    if (ack == LOW) {
      break;
    }
  }

  if (ack == HIGH) 
  {
 SENbus.ACK_Status=1;
  }
 }

/**
 */
uint16_t getData16SHT(void)
{
  uint16_t val;
  // start with idle colock
  digitalWrite(clockPin, LOW);
  // Get the most significant bits
  pinMode(dataPin, INPUT);
//digitalWrite(dataPin, LOW);
  pinMode(clockPin, OUTPUT);
  //val = shiftIn(dataPin, _clockPin, 8);
  val = shiftIn( MSBFIRST );
  val *= 256;

  // Send the required ack
  digitalWrite(dataPin, LOW);
  pinMode(dataPin, OUTPUT);
  shtDelay(1);
  digitalWrite(clockPin, HIGH);
  shtDelay(1);
  digitalWrite(clockPin, LOW);
  shtDelay(1);
  pinMode(dataPin, INPUT);
//digitalWrite(dataPin, LOW);
  // Get the least significant bits
  //val |= shiftIn(_dataPin, _clockPin, 8);
  val |= shiftIn( MSBFIRST);

  return val;
}

/**
 */
uint8_t readCRC(void)
{
	uint8_t crc;

	// Send the required ack
	digitalWrite(dataPin, LOW);
	pinMode(dataPin, OUTPUT);
	shtDelay(1);
	digitalWrite(clockPin, HIGH);
	shtDelay(1);
	digitalWrite(clockPin, LOW);
	shtDelay(1);
	pinMode(dataPin, INPUT);
	//digitalWrite(dataPin, LOW);

	// Get the crc value

	crc = shiftIn( MSBFIRST);

	// end comm, turn off gpio's
	pinMode(dataPin, INPUT);
	pinMode(clockPin, INPUT);

	return crc;
	/*
	uint8_t local_crc = crc8( (uint8_t*)&data, 2);

	if (crc != local_crc){
		Serial.println("SHT: crc error");
		Serial.print(" Got:      0x"); Serial.println(crc, HEX);
		Serial.print(" Expected: 0x"); Serial.println(local_crc, HEX);
	}
	*/
}
// ref http://contiki.sourceforge.net/docs/2.6/a00178_source.html
uint8_t rev8bits(uint8_t v)
{
	uint8_t r = v;
	uint8_t s = 7;

	for (v >>= 1; v; v >>= 1) {
		r <<= 1;
		r |= v & 1;
		s--;
	}
	r <<= s;                  /* Shift when v's highest bits are zero */
	return r;
}
//uint8_t crc8add(uint8_t crc, const uint16_t data)
//{
//	crc = crc8add(crc, (uint8_t)(data >> 8));
//	return crc8add(crc, (uint8_t)(data & 0xff));
//}
uint8_t crc8add(uint8_t crc, const uint8_t data)
{
/*
 * based on contiki's sht11 crc
 * http://contiki.sourceforge.net/docs/2.6/a00178_source.html
 *
 */
  const uint8_t POLYNOMIAL = 0x31;
  uint8_t i;

  crc ^= data;
  for ( i = 8; i > 0; --i ) {
	  crc = ( crc & 0x80 )
				? (crc << 1) ^ POLYNOMIAL
				: (crc << 1);
  }

  return crc;
}

uint8_t shiftIn( int _bitorder)
{
	uint8_t ret = 0;
	uint8_t i;
	uint8_t _numBits = 8;

	for (i=0; i<_numBits; ++i)
	{
		digitalWrite(clockPin, HIGH);
		shtDelay(1);  // I don't know why I need this, but without it I don't get my 8 lsb of temp
		ret = ret*2 + digitalRead(dataPin);

		digitalWrite(clockPin, LOW);
		shtDelay(1);  // I don't know why I need this, but without it I don't get my 8 lsb of temp
	}

	return(ret);
}

/**
 */
uint8_t sendCommandSHT_internal(uint8_t _command)
{
  // Transmission Start
  digitalWrite(dataPin, HIGH);
  digitalWrite(clockPin, LOW);
  pinMode(dataPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  digitalWrite(dataPin, HIGH);
  digitalWrite(clockPin, LOW);

  // start sequence clock LOW and data HIGH
  digitalWrite(clockPin, LOW);
  digitalWrite(dataPin, HIGH);
  shtDelay(2);

  // DATA falls while SCK high
  // SCK falls while DATA low
  // DATA raises while SCK high
  digitalWrite(clockPin, HIGH);
  shtDelay(1);
  digitalWrite(dataPin, LOW);
  shtDelay(1);
  digitalWrite(clockPin, LOW);
  shtDelay(1);
  digitalWrite(clockPin, HIGH);
  shtDelay(1);
  digitalWrite(dataPin, HIGH);
  shtDelay(1);

  // idle clock now goes LOW
  digitalWrite(clockPin, LOW);
  shtDelay(2);

  return shiftOut( MSBFIRST, _command, SHT_ACK);
}
//***************************************************************************************************
uint16_t read_register_Raw(uint8_t REG_ADD )
{
	uint16_t _val = 0x7fff;
	// Command to send to the SHT1x to request regster 
	if (REG_ADD==7)
	{	_val=readStatus();}
	else
	{
		if ( sendCommandSHT(REG_ADD ) == SHT_SUCCESS )
		{
				shtDelay(1);		
 				waitForResultSHT();
				_val = getData16SHT();
		}
		
	}				
	return (_val);
		
}
void write_register_Raw(uint8_t REG_ADD,uint8_t REG_DATA )
{
	uint16_t _val;

	// Command to send to the SHT1x to request regster

	if ( sendCommandSHT(REG_ADD ) == SHT_SUCCESS )
	{
		waitForResultSHT();
	}
	sendCommandSHT_internal(REG_DATA);
	waitForResultSHT();
	
	
		return ;
	
}
