/*
 * UART_to_I2C_Sensibus_Bridge.c
 *
 * Created: 11/14/2021 2:51:43 PM
 * Author : Tom S.
 *
 * UART RX & TX buffers = 64 chars, if required change in uart.h
 */ 

#define TRUE  1
#define FALSE 0

#define F_CPU                 8000000UL
#define UART_BAUD_RATE        9600     // check if compatible with xtal (!)
#define NUM_OF_SUPPORTED_DEVS 5        // per protocol, keep under 10 to avoid code changes :)
#define BUFFER_SIZE           64       // size of local buffer for complete uart message before parsing
#define CURRENT_FW            "00"     // must be string
#define CR            0x0D     


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/atomic.h>
#include <LightweightRingBuff.h>
#include <uart.h>
#include <i2cmaster.h>

void uart_service_routine(); // handles char extraction and uart error reporting
void uart_send_fw_version();
void ring_buffer_insert_string(RingBuff_t* const Buffer, char *s); // extends the ring buffer lib. (adds termination)
void parse_buffer();
uint8_t hex2char (char Data);
char uart_rx_buffer[BUFFER_SIZE];
uint8_t uart_rx_buffer_index = 0;
RingBuff_t uart_tx_buffer;
RingBuff_Data_t uart_tx_buffer_data[BUFFER_SIZE];
uint8_t parse_buffer_bool = FALSE;
uint8_t twi_msg_write = FALSE;
uint8_t twi_msg_read = FALSE;
uint8_t Work_Mode ;
enum {I2C_W =0 , I2C_R =1 ,I2C_C=2  ,Sensibus_W=4  ,Sensibus_R=5, Sensibus_C=6,Board_Query=7 ,IDEL=99 };

// outgoing two wire interface msg
struct TWIMSG
{
	uint16_t id;
	uint16_t address;
	uint16_t msg;
	uint8_t i2c;
} twimsg;
struct Prams
{
    uint16_t id;
	uint16_t SLAVE_address;
	unsigned char Data_Byte[16];
	uint16_t byte_count;
	
} Pram;
struct DeviceData 
{
	uint16_t address;
	uint16_t timeout;
};

struct Data
{
	struct DeviceData idevs_addresses[NUM_OF_SUPPORTED_DEVS];
	struct DeviceData sdevs_addresses[NUM_OF_SUPPORTED_DEVS];
} RAMdata;

int main(void)
{
//	eeprom_read_block(&RAMdata, 0, sizeof(RAMdata));             // read all device data from eeprom
	RingBuffer_InitBuffer(&uart_tx_buffer);                      // init uart tx buffer
	
	uart_init(UART_BAUD_SELECT(UART_BAUD_RATE, F_CPU));
	i2c_init();
	//TODO: sensibus_init();
	
	uart_send_fw_version();
	
	sei();
	
    while(1) 
    {
		uart_service_routine();
		
		if(parse_buffer_bool)
		{
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
			{
				parse_buffer();
				parse_buffer_bool = FALSE;
			}
		}
		
		while(!RingBuffer_IsEmpty(&uart_tx_buffer))
		{
			uart_putc(RingBuffer_Remove(&uart_tx_buffer));
		}
		
		if(twi_msg_write)
		{
			if(twimsg.i2c)
			{
				i2c_start(twimsg.id + I2C_WRITE);
				i2c_write(twimsg.address);
				i2c_write(twimsg.msg);
				i2c_stop();
			}
			else // sensibus write
			{
				
			}
			
			twi_msg_write = FALSE;
		}
		
		if(twi_msg_read)
		{
			if(twimsg.i2c)
			{
				i2c_start_wait(twimsg.id + I2C_WRITE);
				i2c_write(twimsg.address);
				i2c_rep_start(twimsg.id + I2C_READ);
				 i2c_readNak(); // how many bytes to read?
				i2c_stop();
			}
			else // sensibus read 
			{
				
			}
			
			twi_msg_read = FALSE;
		}
	}
}

void uart_service_routine()
{
	uint16_t c = uart_getc();
    
	if(c & UART_NO_DATA)
		return;
	else if(c & UART_FRAME_ERROR)     
		ring_buffer_insert_string(&uart_tx_buffer, "ERR01\0");
    else if(c & UART_OVERRUN_ERROR)   
		ring_buffer_insert_string(&uart_tx_buffer, "ERR02\0");
    else if(c & UART_BUFFER_OVERFLOW) 
		ring_buffer_insert_string(&uart_tx_buffer, "ERR03\0");
	else // insert char in into buffer
	{
		uart_rx_buffer[uart_rx_buffer_index++] = (unsigned char)c;
		ring_buffer_insert_string(&uart_tx_buffer, "N\0");
		if ((unsigned char)c == CR) // string termination -> parse cmd
		{
			parse_buffer_bool = TRUE;
			
		}
	}
}

void inline uart_send_fw_version()
{
	ring_buffer_insert_string(&uart_tx_buffer, "VER");
	ring_buffer_insert_string(&uart_tx_buffer, CURRENT_FW);
	RingBuffer_Insert(&uart_tx_buffer, '\0');
}
//*************************************************************************************

void ring_buffer_insert_string(RingBuff_t* const Buffer, char *s)
{
	for (uint8_t i = 0; i < strlen(s); i++)
	{
		RingBuffer_Insert(Buffer, s[i]);
	}
}
//*************************************************************************************
void parse_buffer()
{
	char local_buffer[BUFFER_SIZE];
	char str[4];
	uint16_t temp_address=0;
	strcpy(local_buffer, uart_rx_buffer); // copy from global to local buffer
	memset(uart_rx_buffer, 0, sizeof(uart_rx_buffer)); // empty global buffer
	uart_rx_buffer_index = 0;                          // reset index
	
	
	
	if			(local_buffer[0]== "i") { Work_Mode=I2C_W;}
	else if		(local_buffer[0]== "b") { Work_Mode=Sensibus_W;}
	else if		(local_buffer[0]== "V") { Work_Mode=Board_Query;}
		
	if 			(local_buffer[1]== "R") { Work_Mode= Work_Mode|0x01;}
	else if 	(local_buffer[1]== "C") { Work_Mode= Work_Mode|0x02;}

temp_address=(hex2char (local_buffer[2]))<<12;
temp_address+=(hex2char (local_buffer[3]))<<8;
temp_address+=(hex2char (local_buffer[4]))<<4;
temp_address+=(hex2char (local_buffer[5]));

Pram.SLAVE_address=temp_address;

memcpy (str,&local_buffer[6],2);
Pram.byte_count=atoi(str);
for(int i=0;i<Pram.byte_count;i++)
	{
	memcpy (str,&local_buffer[8+(i*2)],2);
	Pram.Data_Byte[i]=atoi(str);
	}

	
	}
//*************************************************************************************
uint8_t hex2char (char Data)
{
	uint8_t RETVAL;
		switch ( Data)
			{
				case '0':RETVAL=0;break;
				case '1':RETVAL=1;break;
				case '2':RETVAL=2;break;
				case '3':RETVAL=3;break;
				case '4':RETVAL=4;break;
				case '5':RETVAL=5;break;
				case '6':RETVAL=6;break;
				case '7':RETVAL=7;break;
				case '8':RETVAL=8;break;
				case '9':RETVAL=9;break;
				case 'a':RETVAL=10;break;
				case 'b':RETVAL=11;break;
				case 'c':RETVAL=12;break;
				case 'd':RETVAL=13;break;
				case 'e':RETVAL=14;break;
				case 'f':RETVAL=15;break;
				case 'A':RETVAL=10;break;
				case 'B':RETVAL=11;break;
				case 'C':RETVAL=12;break;
				case 'D':RETVAL=13;break;
				case 'E':RETVAL=14;break;
				case 'F':RETVAL=15;break;
				default:RETVAL=0Xff;break;		
			}
	return		RETVAL;
}
/*	
	
	if(strstr(local_buffer,"who") != NULL)
	{
		uart_send_fw_version();
	}
	else if(strstr(local_buffer, "DEV") != NULL)
	{
		uint8_t dev_index = local_buffer[4] - '0';
		
		if(local_buffer[0] == 'i')
		{
			RAMdata.idevs_addresses[dev_index].timeout = atoi(local_buffer + 6);
			RAMdata.idevs_addresses[dev_index].address = (uint16_t)strtol(local_buffer + 11, NULL, 16);
		}
		else if(local_buffer[0] == 's')
		{
			RAMdata.sdevs_addresses[dev_index].timeout = atoi(local_buffer + 6);
			RAMdata.sdevs_addresses[dev_index].address = (uint16_t)strtol(local_buffer + 11, NULL, 16);
		}
		
		eeprom_update_block(&RAMdata, 0, sizeof(RAMdata)); // update eeprom
		ring_buffer_insert_string(&uart_tx_buffer, "OK");
		RingBuffer_Insert(&uart_tx_buffer, '\0');
	}
	else if(strstr(local_buffer, "SND") != NULL)
	{
		if(local_buffer[0] == 'i')
		{
			twimsg.id = RAMdata.idevs_addresses[local_buffer[4] - '0'].address;
			twimsg.i2c = TRUE;
		}
		else if(local_buffer[0] == 's')
		{
			twimsg.id = RAMdata.sdevs_addresses[local_buffer[4] - '0'].address;
			twimsg.i2c = FALSE;
		}
		
		twimsg.address = (uint16_t)strtol(local_buffer + 6, NULL, 16);
		twimsg.msg = (uint16_t)strtol(local_buffer + 8, NULL, 16);
		twi_msg_write = TRUE;
		
		ring_buffer_insert_string(&uart_tx_buffer, "OK");
		RingBuffer_Insert(&uart_tx_buffer, '\0');
	}
	else if(strstr(local_buffer, "get") != NULL)
	{
		
		//if all data ok
		ring_buffer_insert_string(&uart_tx_buffer, "GOT");
		RingBuffer_Insert(&uart_tx_buffer, '\0');
	}
}
*/

void I2c_Read_Data (void )
{
	i2c_start_wait(Pram.id + I2C_WRITE);
	i2c_write(Pram.SLAVE_address);
	
	for(int i=0;i<Pram.byte_count;i++)
	{
		i2c_rep_start(twimsg.id + I2C_READ);
		i2c_readNak();
	}
	i2c_stop();
}
void I2c_Write_Data (void )
{
	i2c_start(Pram.id + I2C_WRITE);
	i2c_write(Pram.SLAVE_address);
	i2c_write(Pram.Data_Byte);
	i2c_stop();
}

void Sensibus_Write_Data (void )
{
	i2c_start(Pram.id + I2C_WRITE);
	i2c_write(Pram.SLAVE_address);
	i2c_write(Pram.Data_Byte);
	i2c_stop();
}
void Sensibus_Read_Data (void )
{
	i2c_start_wait(Pram.id + I2C_WRITE);
	i2c_write(Pram.SLAVE_address);
	
	for(int i=0;i<Pram.byte_count;i++)
	{
		i2c_rep_start(Pram.id + I2C_READ);
		i2c_readNak();
	}
	i2c_stop();
}