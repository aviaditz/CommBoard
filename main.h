#ifndef MAIN_H
#define MAIN_H

/**@}*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/atomic.h>
#include "LightweightRingBuff.h"
#include "uart.h"
#include "SDI-12.h"

#include "i2cmaster.h"
#include "sensirion.h"

#define TRUE  1
#define FALSE 0

#define F_CPU                 (16000000UL) //(8000000UL)
#define CLOCK_FACTOR        (8) 
#define UART_BAUD_RATE        (19200) //115200  //9600*2     // check if compatible with xtal (!)
#define NUM_OF_SUPPORTED_DEVS 5        // per protocol, keep under 10 to avoid code changes :)
#define BUFFER_SIZE           64       // size of local buffer for complete uart message before parsing
#define CURRENT_FW            "003"    // must be string
#define CR                    0x00

enum 
{
	I2C_W = 0, 
	I2C_R = 1,
	I2C_C = 2,
	Sensibus_W = 4,
	Sensibus_R = 5,
	Sensibus_C = 6,
	Board_Query = 7 ,
	I2C_speed_grade = 8,
	SDI12W = 9,
	SDI12P = 12,
	IDLE = 99,
	RxERROR=255
};

struct Prams
{
	uint8_t ID;
	uint8_t MSB_address; // used for SDI-12 P power ON/OFF command as well
	uint8_t LSB_address;
	unsigned char W_Data[64];
	unsigned char R_Data[64];
	uint8_t byte_count;
	uint8_t ERROR;
	uint8_t SDI12_12Vstate;
	uint8_t SDI_wakeupstatus;
	uint8_t sdi_status;
} Pram;

#define SET_I2C_to_GPIO TWCR&=!TWEN;
#define SET_I2C TWCR!=TWEN;

void I2c_Read_Data (void );
void  send_Sensibus_staus_to_UART(void);
void  send_i2C_read_to_UART(void);	
void  send_i2C_staus_to_UART(void);
void parse_buffer_handler(void);	
void uart_service_routine(); // handles char extraction and uart error reporting
void uart_send_fw_version();
void ring_buffer_insert_string(RingBuff_t* const Buffer, char *s); // extends the ring buffer lib. (adds termination)
void ring_buffer_insert_string_VAR(RingBuff_t* const Buffer, char *s,int size);
void parse_buffer();
unsigned char I2c_Write_Data (void );
void Sensibus_Read_print(void);
void Sensibus_read_Data (void );
void Sensibus_write_Data (void );
uint16_t hex2char (char Data);
void print_VAR(int data);
void print_float(float data);

#endif // MAIN_H