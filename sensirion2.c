/**
 * sensirion_h Library
 *
 * Hydrosense 2016 / hydrosense.net
 *
 * Based on previous work by:
 *    Jonathan Oxer <jon@oxer.com.au> / <www.practicalarduino.com>
 *    Maurice Ribble: <www.glacialwanderer.com/hobbyrobotics/?p=5>
 *    Wayne ?: <ragingreality.blogspot.com/2008/01/ardunio-and-sht15.html>
 *
 * Manages communication with SHT1x series (SHT10, SHT11, SHT15, SHT31)
 * temperature / humidity sensors from Sensirion (www.sensirion.com).
 */

#include "sensirion.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <avr/io.h>
#include <util/delay.h>
#include <util/atomic.h>


#define DataPin_bit 0
#define ClockPin_bit 1
#define  LOW 0
#define 	HIGH 1
#define  OUTPUT 1
#define  INPUT 0
#define MSBFIRST 1
#define LSBFIRST 0
#define SCK_H  PORTC |= 0x20
#define SCK_L  PORTC &= 0xDF
#define SCK_O  DDRC |= 0x20
#define NOP	_delay_us(0.1);
#define SDA_H  PORTC |= 0x10
#define SDA_L  PORTC &= 0xEF
#define SDA_Z  DDRC &= 0xEF
#define SDA_O  DDRC |= 0x10



void Sensibus_Write_Data (void )
{ 

}
//***********************************************************************
void Sensibus_Read_Data (void )
{
uint8_t i;
uint8_t status;

Transmission_Start_sequence();
Transmt_Data(0x0007);
 status= wait_for_ACK();
 if (status!=0){return;}
N_clk (8);
status=	 wait_for_ACK();
if (status!=0){return;}
N_clk (8);
}
//***********************************************************************
void Sensibus_init (void )
{
uint8_t i;
uint16_t Data_Out=0xFCFF;
SCK_O;
SDA_Z;
SCK_L;
//SDA_O;
for ( i =0; i <12; ++i )
{
	SCK_H;
	if 	(Data_Out&1==1){SDA_H;}else {SDA_L;}
	NOP
	SCK_L	;
	NOP
	Data_Out=Data_Out>>1;	
}
}


//***********************************************************************
void Transmission_Start_sequence(void)
{
	SDA_H;
//	SDA_O;
	SCK_O;
NOP
	N_clk (9);
	SDA_H;
	SCK_L;
NOP
	SCK_H;
NOP
	SDA_L;
NOP
	SCK_L;
NOP
	SCK_H;
NOP
	SDA_H;
	NOP
	SCK_L;
	NOP
	SDA_L;
	NOP
}
//***********************************************************************
void Transmt_Data(uint16_t Data_Out)
{
uint8_t i;



for ( i =0; i <8; ++i )
	{
	SCK_H;
	NOP
	if 	((Data_Out & 0x80) !=0)
	{SDA_H;}
		else 
	{SDA_L;}
	NOP
	SCK_L	;
	NOP
	Data_Out=Data_Out<<1;	
	}	
	
}

void N_clk (uint8_t N_CLK)
{
	uint8_t i;
	for ( i =0; i < N_CLK; ++i )
	{
		SCK_H;
	NOP
		SCK_L	;
		NOP
	}
	}
	

void	 digitalWrite(uint8_t Pin, uint8_t level);
{
if (pin==_dataPin)
{
	if (level==HIGH) {SDA_H}else {SDA_L}
}
else 
{
	if (level==HIGH) {SCK_H}else {SCK_L}
}
}
void	 pinMode(uint8_t Pin, uint8_t level);
{
if (pin==_dataPin)
{
	if (level==OUTPUT) {SDA_O}else {SDA_Z}
}
else 
{
	if (level==OUTPUT) {SCK_O}else {SCK_Z}
}
}
	 
